<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="edu.ifsp.lojinha.modelo.Usuario"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lojinha do Zé</title>
</head>

<body>
	<h1>Lojinha do Zé</h1>
	
<%
	Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");

	if(usuario != null){
%>
		<p>Olá <%= usuario.getNome() %>! (<a href="logout">Sair</a>)</p>

<%  } else { %>
		<p><a href="login">Login</a></p>
<%
	}
%>
	
	<p>Faça <a href="cadastro.html">aqui</a> o seu cadastro de cliente!</p>
	
<% 	 if  (usuario != null) { %>
	<p><a href="listar.sec">Listar clientes</a></p>	
<%   } %>
</body>
</html>