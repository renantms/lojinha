package edu.ifsp.lojinha.web;

import java.io.IOException;

import edu.ifsp.lojinha.modelo.Usuario;
import edu.ifsp.lojinha.persistencia.UsuarioDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
		rd.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		
		UsuarioDAO dao = new UsuarioDAO();
		
		Usuario usuario = dao.check(username, password);
		
		if(usuario != null) {
			HttpSession session = request.getSession();
			session.setAttribute("usuario", usuario);
			response.sendRedirect("index.jsp");
		}else {
			request.setAttribute("erro", "Credenciais inválidas.");
			RequestDispatcher rd =  request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
		}
		
	}

}
